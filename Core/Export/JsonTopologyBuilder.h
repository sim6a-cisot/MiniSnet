#ifndef JSONTOPOLOGYBUILDER_H
#define JSONTOPOLOGYBUILDER_H

#include <ConnectionClassifier.h>
#include <PortMatrix.h>
#include <QJsonObject>
#include <QString>

class Switch;
class Host;
class SSLink;

class JsonTopologyBuilder
{
public:
    JsonTopologyBuilder(PortMatrix portMatrix);
    void addSwitchData(Switch *sw);
    void addLinkData(SSLink *link);
    QString buildJson();
private:
    QJsonObject root;
    PortMatrix portMatrix;
    ConnectionClassifier classifier;
    void addMetricsData(QJsonObject & linkJson, SSLink *link);
    void addSSLinkData(SSLink *link);
    void addHSLinkData(SSLink *link);
    Host *getHost(Node *node1, Node *node2);
    Switch *getSwitch(Node *node1, Node *node2);
};

#endif // JSONTOPOLOGYBUILDER_H
