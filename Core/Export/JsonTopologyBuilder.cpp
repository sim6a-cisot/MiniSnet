#include "JsonTopologyBuilder.h"
#include "Switch.h"
#include "Host.h"
#include "SSLink.h"

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <NetworkIdFactory.h>

JsonTopologyBuilder::JsonTopologyBuilder(PortMatrix portMatrix) :
    portMatrix(portMatrix)
{

}

void JsonTopologyBuilder::addSwitchData(Switch *sw)
{
    QJsonArray switchesJson = root["switches"].toArray();
    QJsonObject swJson;
    swJson["dpid"] = sw->getDpid();
    switchesJson.append(swJson);
    root["switches"] = switchesJson;
}

void JsonTopologyBuilder::addLinkData(SSLink *link)
{
    Node * node1 = link->getNode1();
    Node * node2 = link->getNode2();
    if (classifier.isSSConnection(node1, node2)) {
        addSSLinkData(link);
    }
    if (classifier.isHSConnection(node1, node2)) {
        addHSLinkData(link);
    }
}

void JsonTopologyBuilder::addSSLinkData(SSLink *link)
{
    Node * node1 = link->getNode1();
    Node * node2 = link->getNode2();
    Switch * sw1 = dynamic_cast<Switch*>(node1);
    Switch * sw2 = dynamic_cast<Switch*>(node2);
    QJsonArray linksJson = root["links"].toArray();
    QJsonObject linkJson;
    linkJson["sourceDpid"] = sw1->getDpid();
    linkJson["sourcePort"] = portMatrix.getPortNumber(node1, node2);
    linkJson["destinationDpid"] = sw2->getDpid();
    linkJson["destinationPort"] = portMatrix.getPortNumber(node2, node1);
    addMetricsData(linkJson, link);
    linksJson.append(linkJson);
    root["links"] = linksJson;
}

void JsonTopologyBuilder::addHSLinkData(SSLink *link)
{
    Node * node1 = link->getNode1();
    Node * node2 = link->getNode2();
    Host * host = getHost(node1, node2);
    Switch * sw = getSwitch(node1, node2);

    QJsonArray hostsJson = root["hosts"].toArray();
    QJsonObject hostJson;
    hostJson["mac"] = host->getMac();
    hostJson["adjacentDpid"] = sw->getDpid();
    hostJson["adjacentPort"] = portMatrix.getPortNumber(sw, host);
    hostsJson.append(hostJson);
    root["hosts"] = hostsJson;
}

Host *JsonTopologyBuilder::getHost(Node *node1, Node *node2)
{
    if (node1->getDeviceType() == HOST) {
        return dynamic_cast<Host*>(node1);
    }
    return dynamic_cast<Host*>(node2);
}

Switch *JsonTopologyBuilder::getSwitch(Node *node1, Node *node2)
{
    if (node1->getDeviceType() == SWITCH) {
        return dynamic_cast<Switch*>(node1);
    }
    return dynamic_cast<Switch*>(node2);
}

void JsonTopologyBuilder::addMetricsData(QJsonObject & linkJson, SSLink *link)
{
    QJsonObject metricsJson;
    metricsJson["bandwidth"] = link->getBandwidth();
    metricsJson["delay"] = link->getDelay();
    metricsJson["packetLossRate"] = link->getPacketLoss();
    metricsJson["adminMetric"] = link->getAdminMetric();
    linkJson["metrics"] = metricsJson;
}

QString JsonTopologyBuilder::buildJson()
{
    return QJsonDocument(root).toJson(QJsonDocument::Indented);
}

