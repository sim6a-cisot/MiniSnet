#include "Core.h"
#include "IO.h"
#include "XmlDeserializer.h"
#include "DrawingMode.h"

Core::Core(QObject *parent) :
    QObject(parent),
    linkCreator(&map),
    nodeCreator(&map),
    elementEditor(&map)
{
    changeStateToEdit();
    DrawingMode::showDelay();
}

void Core::refreshNetworkMap()
{
    emit signalRefreshNetworkMapView(map.draw());
}

void Core::handleMouseReleaseEvent(QPoint position)
{
    tool->handleMouseUp(position);
    refreshNetworkMap();
}

void Core::handleMousePressEvent(QPoint position)
{
    tool->handleMouseDown(position);
    refreshNetworkMap();
}

void Core::handleMouseMoveEvent(QPoint position)
{
    if (tool->handleMouseMove(position))
    {
        refreshNetworkMap();
    }
}

void Core::handleDoubleClickEvent(QPoint position)
{
    tool->handleMouseDoubleClicked(position);
    refreshNetworkMap();
}

void Core::handleKeyDeletePressEvent()
{
    tool->handleKeyDeletePressEvent();
    refreshNetworkMap();
}

void Core::changeStateToEdit()
{
    tool = &elementEditor;
}

void Core::prepareSdnController()
{
    tool = &nodeCreator;
    nodeCreator.setSdnControllerCreation();
}

void Core::prepareHost()
{
    tool = &nodeCreator;
    nodeCreator.setHostCreation();
}

void Core::prepareSwitch()
{
    tool = &nodeCreator;
    nodeCreator.setSwitchCreation();
}

void Core::prepareLink()
{
    tool = &linkCreator;
}

void Core::showPorts(bool status)
{
    DrawingMode::needShowPorts = status;
    refreshNetworkMap();
}

void Core::showBandwidth()
{
    DrawingMode::showBandwidth();
    refreshNetworkMap();
}

void Core::showDelay()
{
    DrawingMode::showDelay();
    refreshNetworkMap();
}

void Core::showPacketLossRate()
{
    DrawingMode::showPacketLossRate();
    refreshNetworkMap();
}

void Core::createMininetScript(QString filePath)
{
    IO io;
    io.writeFile(map.createMininetScript(), filePath);
}

void Core::createJsonTopology(QString filePath)
{
    IO io;
    io.writeFile(map.createJsonTopology(), filePath);
}

void Core::saveNetworkMap(QString filePath)
{
    IO io;
    io.writeFile(map.createXmlDocument(), filePath);
}

void Core::loadNetworkMap(QString filePath)
{
    IO io;
    XmlDeserializer deserializer;
    QString xmlDocument = io.readFile(filePath);
    deserializer.deserialize(xmlDocument, &map);
    elementEditor.handleMouseDown(QPoint(-999, -999));
    refreshNetworkMap();
}

void Core::eraseMarks()
{
    map.unselectLinks();
    map.unselectNodes();
    refreshNetworkMap();
}

void Core::changeMetric(QVector<float> metricData)
{
    map.changeMetric(metricData);
    refreshNetworkMap();
}

void Core::AlignVertically()
{
    elementEditor.AlignVertically();
    refreshNetworkMap();
}

void Core::AlignHorizontally()
{
    elementEditor.AlignHorizontally();
    refreshNetworkMap();
}

void Core::connectSdnController()
{
    map.connectSdnController();
    refreshNetworkMap();
}

void Core::clearNetworkMap()
{
    elementEditor.handleMouseDown(QPoint(-999, -999));
    map.clear();
    refreshNetworkMap();
}
