#include "NetworkIdFactory.h"

NetworkIdFactory::NetworkIdFactory()
{

}

QString NetworkIdFactory::createMac(int baseNumber)
{
    QString mac = QString("%1").arg(baseNumber, 12, 16, QChar('0'));
    NetworkIdFactory::insertIdDelimeters(mac);
    return mac;
}

QString NetworkIdFactory::createDpid(int baseNumber)
{
    QString dpid = QString("%1").arg(baseNumber, 16, 16, QChar('0'));
    NetworkIdFactory::insertIdDelimeters(dpid);
    return dpid;
}

void NetworkIdFactory::insertIdDelimeters(QString & id)
{
    const int step = 2;
    const QChar delimiter = QChar(':');
    for (int i = step; i < id.size(); i += step+1) {
        id.insert(i, delimiter);
    }
}
