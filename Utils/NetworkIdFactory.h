#ifndef NETWORKIDFACTORY_H
#define NETWORKIDFACTORY_H

#include <QString>


class NetworkIdFactory
{
public:
    NetworkIdFactory();
    static QString createMac(int baseNumber);
    static QString createDpid(int baseNumber);

private:
    static void insertIdDelimeters(QString & id);
};

#endif // NETWORKIDFACTORY_H
